package stringcalc;


import org.junit.jupiter.api.Test;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StringCalculatorTest {

    // "" -> 0
    // "1" -> 1
    // "1, 2" -> 3
    // null -> IllegalArgumentException

    @Test
    public void emptyString_returnsZero() {
        StringCalculator c = new StringCalculator();

        int result = c.add("");

        assertThat(result, is(0));
    }

    @Test
    public void oneNumberString_returnsThatNumber() {
        StringCalculator c = new StringCalculator();

        int result = c.add("1");

        assertThat(result, is(1));
    }

    @Test
    public void TwoNumbersString_returnTheirSum() {
        StringCalculator c = new StringCalculator();

        int result = c.add("1, 2");

        assertThat(result, is(3));
    }

    @Test
    public void null_throwsIllegalArgumentException() {
        StringCalculator c = new StringCalculator();

        assertThrows(IllegalArgumentException.class, () -> {
            c.add(null);
        });
    }

}
