package stack;


import org.junit.jupiter.api.Test;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StackTest {

    private static final int FIRST_ELEMENT = 3;
    private static final int SECOND_ELEMENT = 5;

    @Test
    public void newStackHasNoElements() {
        Stack stack = new Stack(100);
        assertThat(stack.getSize(), is(0));
    }

    @Test
    public void pushingTwoElements_AddsTwoElements() {
        Stack stack = new Stack(10);

        stack.push(FIRST_ELEMENT);
        stack.push(SECOND_ELEMENT);

        assertThat(stack.getSize(), is(2));
    }

    @Test
    public void pushingAndPoppingTwoElements_LeavesEmptyStack() {
        Stack stack = new Stack(10);

        stack.push(FIRST_ELEMENT);
        stack.push(SECOND_ELEMENT);

        stack.pop();
        stack.pop();

        assertThat(stack.getSize(), is(0));
    }

    @Test
    public void poppingTwice_RemovesTwoElements() {
        Stack stack = new Stack(10);

        stack.push(FIRST_ELEMENT);
        stack.push(SECOND_ELEMENT);

        int firstPop = stack.pop();
        int secondPop = stack.pop();

        assertThat(firstPop, is(SECOND_ELEMENT));
        assertThat(secondPop, is(FIRST_ELEMENT));
    }

    @Test
    public void pushingTwoElementsAndPeeking_ReturnsSecondElement() {
        Stack stack = new Stack(10);

        stack.push(FIRST_ELEMENT);
        stack.push(SECOND_ELEMENT);

        int peekedElement = stack.peek();

        assertThat(peekedElement, is(SECOND_ELEMENT));
    }

    @Test
    public void pushingTwoElementsAndPeeking_StackContainsTwoElements() {
        Stack stack = new Stack(10);

        stack.push(FIRST_ELEMENT);
        stack.push(SECOND_ELEMENT);

        stack.peek();

        assertThat(stack.getSize(), is(2));
    }

    @Test
    public void peekingTwice_ReturnsSameElement() {
        Stack stack = new Stack(10);

        stack.push(FIRST_ELEMENT);
        stack.push(SECOND_ELEMENT);

        int firstPeek = stack.peek();
        int secondPeek = stack.peek();

        assertThat(secondPeek, is(firstPeek));
    }

    @Test
    public void poppingEmptyStack_ThrowsException() {
        Stack stack = new Stack(10);

        assertThrows(IllegalStateException.class, () -> stack.pop());
    }

    @Test
    public void peekingEmptyStack_ThrowsException() {
        Stack stack = new Stack(10);

        assertThrows(IllegalStateException.class, () -> stack.peek());
    }

}