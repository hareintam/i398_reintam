package custommock;

import common.BankService;
import common.Money;

public class TestableBankService implements BankService {

    private Money withdrawAmount;
    private Money depositAmount;
    private String depositAccount;
    private String withdrawAccount;
    private boolean fundsAvailable;
    private boolean withdrawCalled = false;

    public boolean wasWithdrawCalledWith(Money money, String account) {
        if (money != null || account != null) {
            if (account.equals(withdrawAccount) && money.equals(withdrawAmount)) {
                return true;
            }
        }
        return false;
    }

    public boolean wasDepositCalledWith(Money money, String account) {
        if (money != null || account != null) {
            if (account.equals(depositAccount) && money.equals(depositAmount)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void withdraw(Money money, String fromAccount) {
        withdrawCalled = true;
        withdrawAmount = money;
        withdrawAccount = fromAccount;

        System.out.println("credit: " + money + " - " + fromAccount);
    }

    @Override
    public void deposit(Money money, String toAccount) {
        depositAmount = money;
        depositAccount = toAccount;

        System.out.println("debit: " + money + " - " + toAccount);
    }

    @Override
    public Money convert(Money money, String targetCurrency) {
        if (money.getCurrency().equals(targetCurrency)) return money;

        double rate = 1.0 / 10;

        return new Money((int) (money.getAmount() / rate), targetCurrency);
    }

    @Override
    public String getAccountCurrency(String account) {
        switch (account) {
            case "E_123":
                return "EUR";
            case "S_456":
                return "SEK";
            default:
                throw new IllegalStateException();
        }
    }

    @Override
    public boolean hasSufficientFundsFor(Money requiredAmount, String account) {
        if (fundsAvailable) {
            return true;
        } else {
            return false;
        }
    }

    public void setSufficientFundsAvailable(boolean areFundsAvailable) {
        if (areFundsAvailable) {
            fundsAvailable = true;
        } else {
            fundsAvailable = false;
        }
    }

    public boolean wasWithdrawCalled() {
        return withdrawCalled;
    }

}