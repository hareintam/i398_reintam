package invoice;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

class InvoiceController {

    private InvoiceRowDaoImpl invoiceRowDao = new InvoiceRowDaoImpl();
    private InvoiceRowGenerator generator = new InvoiceRowGenerator();

    void generateInvoiceRowsAndSaveToDb(BigDecimal amount, LocalDate periodStart, LocalDate periodEnd) {
        List<InvoiceRow> invoiceRows = generator.generateInvoiceRows(amount, periodStart, periodEnd);
        for (InvoiceRow invoiceRow : invoiceRows) {
            invoiceRowDao.save(invoiceRow);
        }
    }
}
