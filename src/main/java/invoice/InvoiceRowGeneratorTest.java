package invoice;

import org.junit.jupiter.api.Test;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

class InvoiceRowGeneratorTest {

    @Test
    void shouldGenerateInvoiceRowsWithCorrectDates() {
        BigDecimal amount = new BigDecimal(100);
        LocalDate firstDayOfMonth = asDate("2017-01-01");
        LocalDate oneDayAndOneMonthLater = asDate("2017-02-02");
        LocalDate notFirstDayOfMonth = asDate("2017-01-02");
        LocalDate twoMonthsLater = asDate("2017-03-02");

        InvoiceRowGenerator generator = new InvoiceRowGenerator();
        List<InvoiceRow> twoInvoiceRows = generator.generateInvoiceRows(amount, firstDayOfMonth, oneDayAndOneMonthLater);
        List<InvoiceRow> threeInvoiceRows = generator.generateInvoiceRows(amount, notFirstDayOfMonth, twoMonthsLater);

        assertThat(twoInvoiceRows.size(), is(2));
        assertThat(twoInvoiceRows.get(0).getDateString(), is("2017-01-01"));
        assertThat(twoInvoiceRows.get(1).getDateString(), is("2017-02-01"));

        assertThat(threeInvoiceRows.size(), is(3));
        assertThat(threeInvoiceRows.get(0).getDateString(), is("2017-01-02"));
        assertThat(threeInvoiceRows.get(1).getDateString(), is("2017-02-01"));
        assertThat(threeInvoiceRows.get(2).getDateString(), is("2017-03-01"));
    }

    @Test
    void shouldGenerateInvoiceRowsWithCorrectAmounts() {
        BigDecimal dividesEvenly = new BigDecimal(9);
        BigDecimal doesNotDivideEvenly = new BigDecimal(11);
        BigDecimal lessThanMinAmount = new BigDecimal(2);
        BigDecimal smallAmount = new BigDecimal(7);
        LocalDate periodStart = asDate("2017-01-02");
        LocalDate periodEnd = asDate("2017-03-01");
        LocalDate periodEndLonger = asDate("2017-04-04");

        InvoiceRowGenerator generator = new InvoiceRowGenerator();
        List<InvoiceRow> evenInvoiceRows = generator.generateInvoiceRows(dividesEvenly, periodStart, periodEnd);
        List<InvoiceRow> notEvenInvoiceRows = generator.generateInvoiceRows(doesNotDivideEvenly, periodStart, periodEnd);
        List<InvoiceRow> oneInvoiceRow = generator.generateInvoiceRows(lessThanMinAmount, periodStart, periodEnd);
        List<InvoiceRow> smallAmountInvoiceRows = generator.generateInvoiceRows(smallAmount, periodStart, periodEndLonger);

        assertThat(evenInvoiceRows.size(), is(3));
        assertThat(evenInvoiceRows.get(0).getAmountInt(), is(3));
        assertThat(evenInvoiceRows.get(1).getAmountInt(), is(3));
        assertThat(evenInvoiceRows.get(2).getAmountInt(), is(3));

        assertThat(notEvenInvoiceRows.size(), is(3));
        assertThat(notEvenInvoiceRows.get(0).getAmountInt(), is(3));
        assertThat(notEvenInvoiceRows.get(1).getAmountInt(), is(4));
        assertThat(notEvenInvoiceRows.get(2).getAmountInt(), is(4));

        assertThat(oneInvoiceRow.size(), is(1));
        assertThat(oneInvoiceRow.get(0).getAmountInt(), is(2));

        assertThat(smallAmountInvoiceRows.size(), is(2));
        assertThat(smallAmountInvoiceRows.get(0).getAmountInt(), is(3));
        assertThat(smallAmountInvoiceRows.get(1).getAmountInt(), is(4));
    }

    static LocalDate asDate(String string) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return LocalDate.parse(string, formatter);
    }
}