package invoice;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.Collections.*;

class InvoiceRowGenerator {

    private final BigDecimal MIN_AMOUNT = new BigDecimal(3);

    List<InvoiceRow> generateInvoiceRows(BigDecimal amount, LocalDate periodStart, LocalDate periodEnd) {
        int numberOfMonths = getNumberOfMonths(periodStart, periodEnd);
        if (numberOfMonths > 1 && canDivide(amount)) {
            List<BigDecimal> monthlyAmounts = getMonthlyAmounts(amount, numberOfMonths);
            List<LocalDate> invoiceDates = getInvoiceDates(monthlyAmounts.size(), periodStart);
            return populateInvoiceRows(monthlyAmounts, invoiceDates);
        } else {
            return singletonList(new InvoiceRow(amount, periodStart));
        }
    }


    private List<BigDecimal> getMonthlyAmounts(BigDecimal amount, int months) {
        int canBeDividedInto = amount.intValue() / MIN_AMOUNT.intValue();
        if (months <= canBeDividedInto) {
            return getAmounts(amount, months);
        } else {
            return getAmounts(amount, canBeDividedInto);
        }
    }

    private List<BigDecimal> getAmounts(BigDecimal amount, int numberOfMonths) {
        List<BigDecimal> dividedAmounts = new ArrayList<>();
        int equalAmount = amount.intValue() / numberOfMonths;
        int totalRemainder = amount.intValue() % numberOfMonths;
        int secondLastIndex = numberOfMonths - 2;
        for (int i = 0; i < numberOfMonths; i++) {
            if (i >= secondLastIndex && totalRemainder > 0) {
                if (totalRemainder % 2 == 0 || i == secondLastIndex) {
                    dividedAmounts.add(BigDecimal.valueOf(equalAmount).add(BigDecimal.valueOf(totalRemainder / 2)));
                } else {
                    dividedAmounts.add(BigDecimal.valueOf(equalAmount).add(BigDecimal.valueOf((totalRemainder / 2)+ 1)));
                }
            } else {
                dividedAmounts.add(BigDecimal.valueOf(equalAmount));
            }
        }
        return dividedAmounts;
    }

    private List<LocalDate> getInvoiceDates(int numberOfMonths, LocalDate periodStart) {
        List<LocalDate> invoiceDates = new ArrayList<>();
        invoiceDates.add(periodStart);
        LocalDate nextDate;
        for (int i = 1; i < numberOfMonths; i++) {
            nextDate = LocalDate.of(periodStart.getYear(), periodStart.getMonthValue() + i, 1);
            invoiceDates.add(nextDate);
        }
        return invoiceDates;
    }

    private List<InvoiceRow> populateInvoiceRows(List<BigDecimal> amounts, List<LocalDate> dates) {
        return IntStream.range(0, amounts.size())
                .mapToObj(i -> new InvoiceRow(amounts.get(i), dates.get(i)))
                .collect(Collectors.toList());
    }

    private int getNumberOfMonths(LocalDate periodStart, LocalDate periodEnd) {
        int years = periodEnd.getYear() - periodStart.getYear();
        int months = periodEnd.getMonthValue() - periodStart.getMonthValue() + 1;
        return years * 12 + months;
    }

    private boolean canDivide(BigDecimal amount) {
        return amount.compareTo(MIN_AMOUNT) == 1;
    }
}
