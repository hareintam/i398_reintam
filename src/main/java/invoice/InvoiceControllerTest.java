package invoice;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;

import static invoice.InvoiceRowGeneratorTest.asDate;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class InvoiceControllerTest {

    @Mock private InvoiceRowDaoImpl invoiceRowDaoImpl;
    @Mock private InvoiceRowGenerator invoiceRowGenerator;
    @InjectMocks private InvoiceController invoiceController;

    @BeforeEach
    void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void shouldSaveInvoiceRows() {
        BigDecimal amount = new BigDecimal(100);
        LocalDate periodStart = asDate("2017-01-01");
        LocalDate periodEnd = asDate("2017-02-01");

        InvoiceRow firstInvoiceRow = new InvoiceRow(new BigDecimal(50), periodStart);
        InvoiceRow secondInvoiceRow = new InvoiceRow(new BigDecimal(50), periodEnd);

        when(invoiceRowGenerator.generateInvoiceRows(amount, periodStart, periodEnd))
                .thenReturn(Arrays.asList(firstInvoiceRow,secondInvoiceRow));

        invoiceController.generateInvoiceRowsAndSaveToDb(amount, periodStart, periodEnd);

        verify(invoiceRowDaoImpl).save(firstInvoiceRow);
        verify(invoiceRowDaoImpl).save(secondInvoiceRow);
    }
}
