package invoice;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDate;

@EqualsAndHashCode
@AllArgsConstructor
class InvoiceRow {

    private BigDecimal amount;
    private LocalDate date;

    String getDateString() {
        return date.toString();
    }

    int getAmountInt() { return amount.intValue(); }

    @Override
    public String toString() {
        return "InvoiceRow{" +
                "amount=" + amount +
                ", date=" + date +
                '}';
    }
}