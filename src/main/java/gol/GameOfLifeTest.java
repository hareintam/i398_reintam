package gol;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class GameOfLifeTest {

    private Frame frame;
    private int aliveNeighbours;

    @BeforeEach
    public void createFrame() {
        frame = new Frame(5,5);
    }

    @Test
    public void shouldMarkCellAsAlive() {
        frame.markAlive(0, 1);

        assertThat(frame.isAlive(0,1), is(true));
    }

    @Test
    public void shouldReturnTwoIfCellHasTwoLivingNeighbours() {
        frame.markAlive(1,0);
        frame.markAlive(2,1);
        frame.markAlive(3,1);

        aliveNeighbours = frame.getNeighbourCount(2,1);

        assertThat(aliveNeighbours, is(2));
    }

    @Test
    public void shouldReturnTrueIfTwoFramesHaveSameSizeAndCellPositions() {
        frame.markAlive(0,0);
        frame.markAlive(0,1);

        Frame toCompare = new Frame(5,5);
        toCompare.markAlive(0,0);
        toCompare.markAlive(0,1);

        assertThat(frame.equals(toCompare), is(true));
    }

    @Test
    public void shouldReturnFalseIfTwoFramesAreSameSizeButHaveDifferentCells() {
        frame.markAlive(0,0);
        frame.markAlive(0,1);

        Frame toCompare = new Frame(5,5);
        toCompare.markAlive(4,0);
        toCompare.markAlive(0,3);

        assertThat(frame.equals(toCompare), is(false));
    }

    @Test
    public void shouldReturnFalseIfTwoFramesHaveDifferentSize() {
        frame.markAlive(0,0);
        frame.markAlive(0,1);

        Frame toCompare = new Frame(6,5);
        toCompare.markAlive(0,0);
        toCompare.markAlive(0,1);

        assertThat(frame.equals(toCompare), is(false));
    }

    @Test
    public void shouldReturnEightIfAllNeighboursAreAlive() {
        frame.markAlive(2,2);

        frame.markAlive(1,2);
        frame.markAlive(3,2);
        frame.markAlive(2,1);
        frame.markAlive(2,3);
        frame.markAlive(1,1);
        frame.markAlive(3,1);
        frame.markAlive(1,3);
        frame.markAlive(3,3);

        aliveNeighbours = frame.getNeighbourCount(2,2);

        assertThat(aliveNeighbours, is(8));
    }

    @Test
    public void shouldCountAll8LivingNeighboursForCellInCorner() {
        frame.markAlive(0,4);

        frame.markAlive(0,0);
        frame.markAlive(1,0);
        frame.markAlive(4,0);
        frame.markAlive(0,3);
        frame.markAlive(1,3);
        frame.markAlive(1,4);
        frame.markAlive(4,3);
        frame.markAlive(4,4);

        aliveNeighbours = frame.getNeighbourCount(0,4);

        assertThat(aliveNeighbours, is(8));
    }

    @Test
    public void shouldReturnZeroIfCellHasNoLivingNeighbours() {
        frame.markAlive(1,0);
        frame.markAlive(0,0);
        frame.markAlive(4,4);

        aliveNeighbours = frame.getNeighbourCount(2,3);

        assertThat(aliveNeighbours, is(0));
    }

    @Test
    public void shouldReturnStringRepresentationOfCells() {
        boolean[] cells = new boolean[]{true, false, true, false, false};
        String expectedCells = "O  X  O  X  X" + "\n";

        assertThat(frame.getPrintingFormat(cells), is(expectedCells));
    }

    @Test
    public void shouldPrintOut3X3FrameWithCells() {
        Frame smallFrame = new Frame(3,3);
        smallFrame.markAlive(0,1);
        smallFrame.markAlive(1,0);
        smallFrame.markAlive(2,2);

        String expectedCells = "X  O  X\n" +
                               "O  X  X\n" +
                               "X  X  O\n\n";

        assertThat(smallFrame.toString(), is(expectedCells));
    }

    @Test
    public void threeCellBlinkerShouldHavePeriod2() {
        frame.markAlive(2,1);
        frame.markAlive(2,2);
        frame.markAlive(2,3);

        Frame secondFrame = frame.nextFrame();
        aliveNeighbours = secondFrame.getNeighbourCount(2,2);

        Frame thirdFrame = secondFrame.nextFrame();
        Frame fourthFrame = thirdFrame.nextFrame();

        assertThat(aliveNeighbours, is(2));
        assertThat(secondFrame.isAlive(1,2), is(true));
        assertThat(secondFrame.isAlive(3,2), is(true));
        assertThat(frame.equals(thirdFrame), is(true));
        assertThat(secondFrame.equals(fourthFrame), is(true));
    }

    @Test
    public void gliderOf5CellsShouldMoveAcrossLivingSpace() {
        frame.markAlive(1,0);
        frame.markAlive(2,1);
        frame.markAlive(0,2);
        frame.markAlive(1,2);
        frame.markAlive(2,2);

        Frame secondFrame = frame.nextFrame();
        Frame thirdFrame = secondFrame.nextFrame();
        Frame fourthFrame = thirdFrame.nextFrame();

        assertThat(frame.equals(secondFrame), is(false));
        assertThat(secondFrame.equals(thirdFrame), is(false));
        assertThat(thirdFrame.equals(fourthFrame), is(false));
    }

    @Test
    public void blockOf4CellsShouldStayTheSameInNextFrames() {
        frame.markAlive(0,0);
        frame.markAlive(0,1);
        frame.markAlive(1,0);
        frame.markAlive(1,1);

        Frame secondFrame = frame.nextFrame();
        Frame thirdFrame = secondFrame.nextFrame();

        assertThat(frame.equals(secondFrame), is(true));
        assertThat(secondFrame.equals(thirdFrame), is(true));
    }
}