package gol;

import java.util.Arrays;

public class Frame {

    private int width, height;
    private boolean[][] livingSpace;

    public Frame(int width, int height) {
        this.width = width;
        this.height = height;
        this.livingSpace = new boolean[width][height];
    }

    @Override
    public String toString() {
        String livingSpaceString = "";
        for (boolean[] cells : livingSpace) {
            livingSpaceString += getPrintingFormat(cells);
        }
        livingSpaceString += "\n";
        return livingSpaceString;
    }

    String getPrintingFormat(boolean[] cells) {
        String formattedCells = Arrays.toString(cells).replace("true", "O").replace("false", "X").replace("[", "").replace("]", "").replace(",", " ");
        return formattedCells + "\n";
    }

    public boolean equals(Frame toCompare) {
        return Arrays.deepEquals(livingSpace, toCompare.livingSpace);
    }

    //The edges are considered to be connected
    public Integer getNeighbourCount(int x, int y) {
        int livingNeighbours = 0;
        for (int xCoord = (x-1); xCoord <= (x+1) ; xCoord++) {
            for (int yCoord = (y-1); yCoord <= (y+1) ; yCoord++) {
                if (xCoord != x || yCoord != y) {
                    if (isAlive(xCoord,yCoord)) {
                        livingNeighbours++;
                    }
                }
            }
        }
        return livingNeighbours;
    }

    public boolean isAlive(int x, int y) {
        int nonNegativeX = (x % width + width) % width;
        int nonNegativeY = (y % height + height) % height;
        return livingSpace[nonNegativeX][nonNegativeY];
    }

    public void markAlive(int x, int y) {
        livingSpace[x][y] = true;
    }

    public Frame nextFrame() {
        Frame nextFrame = new Frame(width, height);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int livingNeighbours = getNeighbourCount(x, y);
                if (isAlive(x, y) && livingNeighbours == 2 || livingNeighbours == 3) {
                    nextFrame.livingSpace[x][y] = true;
                }
            }
        } return nextFrame;
    }

}