package refactoring;

import java.util.*;

public class Refactoring {

    private static final double VAT = 1.2;

    // 1a
    public int nextInvoiceNumber() {
        return ++invoiceNumber;
    }

    // 1b
    public void addFilledOrdersTo(List<Order> orderList) {
        for (Order order : orders) {
            if (order.isFilled()) {
                orderList.add(order);
            }
        }
    }

    // 2a
    public void printStatementFor(Long invoiceId) {
        List<InvoiceRow> invoiceRows = dao.getInvoiceRowsFor(invoiceId);

        printInvoiceRows(invoiceRows);

        double total = calculateTotal(invoiceRows);

        printValue(total);
    }

    public double calculateTotal(List<InvoiceRow> invoiceRows) {
        double total = 0;
        for (InvoiceRow invoiceRow : invoiceRows) {
            total += invoiceRow.getAmount();
        }
        return total;
    }

    // 2b
    public String getItemsAsHtml() {
        String result = "<ul>";
        for (String s : items) {
            result += createTag("li", s);
        }
        return result += "</ul>";
    }

    public String createTag(String tag, String body) {
        return "<" + tag + ">" + body + "</" + tag + ">";
    }
    
    // 3
    public boolean isSmallOrder() {
        return order.getTotal() > 100;
    }

    // 4
    public void printPrice() {

        double basePrice = getBasePrice();
        System.out.println("Price not including VAT: " + basePrice);
        System.out.println("Price including VAT: " + includingVAT(basePrice));
    }

    private double includingVAT(double basePrice) {
        return basePrice * VAT;
    }

    // 5
    public void calculatePayFor(Job job) {
        boolean weekend = (job.day == 6 || job.day == 7);
        boolean workHours = ( job.hour > 6 ||job.hour < 21);
        if ( weekend && !workHours)  {

        }
    }

    // 6
    public boolean canAccessResource(SessionData sessionData) {
        return isAdminWithPreferredStatus(sessionData);
    }

    private boolean isAdminWithPreferredStatus(SessionData sessionData) {
        return (sessionData.getCurrentUserName().equals("Admin")
            || sessionData.getCurrentUserName().equals("Administrator"))
        && (sessionData.getStatus().equals("preferredStatusX")
            || sessionData.getStatus().equals("preferredStatusY"));
    }

    // 7
    public void drawLines() {
        Space space = new Space();
        Point startPoint1 = new Point(12, 2,5);
        Point endPoint1 = new Point(2,4,6);
        space.drawLine(startPoint1, endPoint1);

        Point startPoint2 = new Point(2,4,6);
        Point endPoint2 = new Point(0,1,0);
        space.drawLine(startPoint2, endPoint2);
    }

    // 8
    public int calculateWeeklyPayWithOvertime(int hoursWorked) {
        if (hoursWorked > 40) {
            int straightPay = calculateWeeklyPayWithoutOvertime(40);

            int overTime = hoursWorked - 40;
            double overtimeRate = 1.5 * hourRate;
            int overTimePay = (int) Math.round(overTime * overtimeRate);
            return straightPay + overTimePay;
        } else {
            return calculateWeeklyPayWithoutOvertime(hoursWorked);
        }
    }

    public int calculateWeeklyPayWithoutOvertime(int hoursWorked) {
        return hoursWorked * hourRate;
    }

    // //////////////////////////////////////////////////////////////////////////

    // Helper fields and methods.
    // They are here just to make code compile

    private List<String> items = Arrays.asList("1", "2", "3", "4");
    private int hourRate = 5;
    int invoiceNumber = 0;
    private List<Order> orders = new ArrayList<>();
    private Order order = new Order();
    private Dao dao = new SampleDao();
    private double price = 0;

    void justACaller() {
        nextInvoiceNumber();
        addFilledOrdersTo(null);
    }

    private void printValue(double total) {
    }

    private void printInvoiceRows(List<InvoiceRow> invoiceRows) {
    }

    class Space {

        public void drawLine(Point startPoint, Point endPoint) {
        }

    }

    class Point {
        private int x, y, z;

        public Point(int onXAxis, int onYAxis, int onZAxis) {
            x = onXAxis;
            y = onYAxis;
            z = onZAxis;
        }
    }

    interface Dao {
        List<InvoiceRow> getInvoiceRowsFor(Long invoiceId);
    }

    class SampleDao implements Dao {
        @Override
        public List<InvoiceRow> getInvoiceRowsFor(Long invoiceId) {
            return null;
        }
    }

    class Order {
        public boolean isFilled() {
            return false;
        }

        public double getTotal() {
            return 0;
        }
    }

    class InvoiceRow {
        public double getAmount() {
            return 0;
        }
    }

    class Job {
        public int hour;
        public int day;
    }

    private double getBasePrice() {
        return price;
    }

    private class SessionData {

        public String getCurrentUserName() {
            return null;
        }

        public String getStatus() {
            return null;
        }

    }

}
