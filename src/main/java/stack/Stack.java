package stack;

import java.util.Arrays;

public class Stack {

    private int[] stack;
    private int noOfElements = 0;

    public Stack() {}

    public Stack(int size) {
        this.stack = new int[size];
    }

    public int getSize() {
        return noOfElements;
    }

    public void push(int i) {
        if (noOfElements < stack.length) {
            stack[noOfElements] = i;
            noOfElements++;
        }
    }

    public int pop() {
        try {
            int lastElement = stack[noOfElements - 1];
            noOfElements--;
            return lastElement;
        } catch (Exception e) {
            throw new IllegalStateException();
        }
    }

    public int peek() {
        try {
            return stack[noOfElements - 1];
        } catch (Exception e) {
            throw new IllegalStateException();
        }
    }
}