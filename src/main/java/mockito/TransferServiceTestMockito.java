package mockito;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import common.BankService;
import common.TransferService;
import common.Money;

@SuppressWarnings("unused")
public class TransferServiceTestMockito {

    BankService bankService = mock(BankService.class);
    TransferService transferService = new TransferService(bankService);

    @Test
    public void transferWithCurrencyConversion() {
        Money oneEuro = new Money(1, "EUR");
        Money oneEuroInSek = new Money(1, "SEK");

        when(bankService.getAccountCurrency("E_123")).thenReturn("EUR");
        when(bankService.convert(oneEuro, "EUR")).thenReturn(oneEuro);
        when(bankService.hasSufficientFundsFor(oneEuro, "E_123")).thenReturn(true);
        when(bankService.getAccountCurrency("S_456")).thenReturn("SEK");
        when(bankService.convert(oneEuro, "SEK")).thenReturn(oneEuroInSek);

        // transfer 1 EUR from account E_123 to account S_456
        // E_123 accounts currency is EUR
        // S_456 accounts currency is SEK
        transferService.transfer(oneEuro, "E_123", "S_456");

        verify(bankService).withdraw(oneEuro, "E_123");
        verify(bankService).deposit(oneEuroInSek, "S_456");
    }

    @Test
    public void transferWhenNotEnoughFunds() {
        Money oneEuro = new Money(1, "EUR");

        when(bankService.getAccountCurrency("E_123")).thenReturn("EUR");
        when(bankService.convert(oneEuro, "EUR")).thenReturn(oneEuro);
        when(bankService.hasSufficientFundsFor(oneEuro, "E_123")).thenReturn(false);

        transferService.transfer(oneEuro, "E_123", "S_456");

        verify(bankService, never()).withdraw(anyMoney(), anyAccount());
    }

    private Money anyMoney() {
        return (Money) any();
    }

    private String anyAccount() {
        return anyString();
    }
}