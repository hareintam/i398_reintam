package stringcalc;

public class StringCalculator {

    public StringCalculator() {
    }

    public int add(String s) {
        int result = 0;

        if (s == null) {
            throw new IllegalArgumentException();
        } else if (s.length() > 0) {
            String[] stringNumbers = s.split(",");
            for (String number : stringNumbers) {
                result += Integer.parseInt(number.trim());
            }
        }
        return result;
    }
}
