package string;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
public class OrderService {

    private DataSource dataSource;

    public OrderService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<Order> getFilledOrders() {
        return dataSource.getOrders().stream()
                .filter(o -> o.isFilled())
                .collect(Collectors.toList());
    }

    public List<Order> getOrdersOver(double amount) {
        return dataSource.getOrders().stream()
                .filter(o -> o.getTotal() > amount)
                .collect(Collectors.toList());
    }

    public List<Order> getOrdersSortedByDate() {
        return dataSource.getOrders().stream()
                .sorted(Comparator.comparing(Order::getOrderDate))
                .collect(Collectors.toList());

    }
}
