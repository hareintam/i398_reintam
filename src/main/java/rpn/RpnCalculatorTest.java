package rpn;

import org.junit.jupiter.api.Test;
//import stack.Stack;

import java.util.Stack;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class RpnCalculatorTest {

    @Test
    public void newCalculatorHasZeroInItsAccumulator() {
        RpnCalculator c = new RpnCalculator();

        assertThat(c.getAccumulator(), is(0));
    }

    @Test
    public void settingAccumulatorSetsItToThatValue() {
        RpnCalculator c = new RpnCalculator();
        c.setAccumulator(5);

        assertThat(c.getAccumulator(), is(5));
    }

    @Test
    public void addingOneAndTwoResultsInThree() {
        RpnCalculator c = new RpnCalculator();

        c.setAccumulator(1);
        c.enter();
        c.setAccumulator(2);
        c.plus();

        assertThat(c.getAccumulator(), is(3));

    }

    @Test
    public void addingTwoValuesAndMultiplying() {
        RpnCalculator c = new RpnCalculator();

        c.setAccumulator(1);
        c.enter();
        c.setAccumulator(2);
        c.plus();
        c.enter();
        c.setAccumulator(4);
        c.multiply();

        assertThat(c.getAccumulator(), is(12));
    }

    @Test
    public void multiplyingAddedValues() {
        RpnCalculator c = new RpnCalculator();

        c.setAccumulator(3);
        c.enter();
        c.setAccumulator(4);
        c.plus();
        c.enter();
        c.setAccumulator(2);
        c.enter();
        c.setAccumulator(1);
        c.plus();
        c.multiply();

        assertThat(c.getAccumulator(), is(21));
    }

}

class RpnCalculator {

    private int accumulator = 0;
    private Stack stack = new Stack();

    public Integer getAccumulator() {
        return accumulator;
    }

    public void setAccumulator(int accumulator) {
        this.accumulator = accumulator;
    }

    public void enter() {
        stack.push(getAccumulator());
        setAccumulator(0);
    }

    public void plus() {
        int result = (int) stack.pop() + getAccumulator();
        setAccumulator(result);
    }

    public void minus() {
        int result = (int) stack.pop() - getAccumulator();
        setAccumulator(result);
    }

    public void multiply() {
        int result = (int) stack.pop() * getAccumulator();
        setAccumulator(result);
    }

}
